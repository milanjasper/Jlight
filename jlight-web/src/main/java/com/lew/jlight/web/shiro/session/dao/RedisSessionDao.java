package com.lew.jlight.web.shiro.session.dao;

import com.google.common.collect.Sets;

import com.lew.jlight.redis.RedisLite;
import com.lew.jlight.web.shiro.util.SerializeUtil;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import javax.annotation.Resource;

public class RedisSessionDao extends AbstractSessionDAO {
    private static Logger logger = LoggerFactory.getLogger(RedisSessionDao.class);

    private final String REDIS_SHIRO_SESSION = "shiro-session:";

    @Resource
    private RedisLite redisLite;

    @Override
    public void delete(Session session) {
        if (session == null) {
            logger.error("session or session id is null");
            return;
        }
        Serializable id = session.getId();
        if (id != null) {
            redisLite.deleteByKey(getByteKey(id));
        }
    }
    @Override
    public Collection<Session> getActiveSessions() {
        Set<Session> sessions = Sets.newHashSet();
        Set<String> byteKeys = redisLite
                .keys(this.REDIS_SHIRO_SESSION + "*");
        if (byteKeys != null && byteKeys.size() > 0) {
            for (String key : byteKeys) {
                Session s = (Session) SerializeUtil.deserialize(redisLite.getByteArray(key));
                sessions.add(s);
            }
        }
        return sessions;
    }

    @Override
    public void update(Session session) throws UnknownSessionException {
        if (session == null || session.getId() == null) {
            logger.error("session or session id is null");
            return;
        }
        String key = this.getByteKey(session.getId());
        byte[] value = SerializeUtil.serialize(session);
        Long timeOut = session.getTimeout() / 1000;
        redisLite.setByteArray(key, value, timeOut.intValue());
    }

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        if (session.getId() == null) {
            logger.error("session or session id is null");
            return null;
        }
        byte[] value = SerializeUtil.serialize(session);
        Long timeOut = session.getTimeout() / 1000;
        redisLite.setByteArray(getByteKey(session.getId()), value, timeOut.intValue());
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        if (sessionId==null) {
            logger.error("session id is null");
            return null;
        }
        Session session;
        String key = getByteKey(sessionId);
        byte[] value = redisLite.getByteArray(key);
        if (value==null || value.length==0){
            return null;
        }
        session = (Session) SerializeUtil.deserialize(value);
        return session;
    }

    private String getByteKey(Serializable sessionId) {
        return this.REDIS_SHIRO_SESSION + sessionId;
    }

}
