package com.lew.jlight.web.controller;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

import com.lew.jlight.core.IdGenerator;
import com.lew.jlight.core.Response;
import com.lew.jlight.core.page.Page;
import com.lew.jlight.mybatis.ParamFilter;
import com.lew.jlight.web.aop.annotaion.WebLogger;
import com.lew.jlight.web.entity.Dept;
import com.lew.jlight.web.service.DeptService;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import javax.annotation.Resource;

import static com.google.common.base.Preconditions.checkArgument;


@Controller
@RequestMapping("dept")
public class DeptController {

    @Resource
    private DeptService depService;

    @GetMapping("list")
    public String list() {
        return "system/dictList";
    }

    @ResponseBody
    @PostMapping("list")
    @WebLogger("查询字典列表")
    public Response list(@RequestBody ParamFilter queryFilter) {
        List<Dept> list = depService.getList(queryFilter);
        Page page = queryFilter.getPage();
        return new Response(list, page);
    }

    @ResponseBody
    @PostMapping("add")
    @WebLogger("添加字典")
    public Object add(@RequestBody Dept dept) {
        Preconditions.checkNotNull(dept, "不能为空");
        Response response = new Response();
        if (Strings.isNullOrEmpty(dept.getDeptId())) {
            dept.setDeptId(IdGenerator.getInstance().nextId());
            depService.add(dept);
        } else {
            depService.update(dept);
        }
        response.setMsg("添加成功");
        return response;
    }
    
    @ResponseBody
    @GetMapping("detail")
    @WebLogger("查询字典详细")
    public Response detail(String id) {
        Preconditions.checkNotNull(id, "不能为空");
        Dept dept = depService.getById(id);
        return new Response(dept);
    }


	@ResponseBody
    @PostMapping("delete")
    @WebLogger("删除字典")
    public Response delete(@RequestBody List<String> ids) {
		checkArgument((ids != null && ids.size() > 0), "不能为空");
        depService.delete(ids);
        return new Response();
    }

    @ResponseBody
    @GetMapping("getTree")
    public Object getTree(){
        return depService.getTree();
    }
    @ResponseBody
    @GetMapping("getByParentId")
    public Response getByParentId(String parentId){
        List<Dept> deptList = depService.getListByParentId(parentId);
        return new Response(deptList);
    }

    @ResponseBody
    @GetMapping("getCatagory")
    public Response getCatagory(){
    	List<Dept> list = depService.getCatagory();
    	Response response = new Response();
    	response.setData(list);
    	return response;
    } 
}
