package com.lew.jlight.web.dao;


import com.lew.jlight.mybatis.dao.BaseDao;
import com.lew.jlight.web.entity.Dept;

import org.springframework.stereotype.Repository;

@Repository
public class DeptDao extends BaseDao<Dept> {

}
