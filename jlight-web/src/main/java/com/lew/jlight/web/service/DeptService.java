package com.lew.jlight.web.service;


import com.lew.jlight.mybatis.ParamFilter;
import com.lew.jlight.web.entity.Dept;
import com.lew.jlight.web.entity.pojo.JSTree;

import java.util.List;

public interface DeptService {

	void add(Dept dept);

	List<Dept> getList(ParamFilter queryFilter);

	void update(Dept dept);

	List<JSTree> getTree();

	List<Dept> getListByParentId(String parentId);

	List<Dept> getCatagory();

	void delete(List<String> id);

    Dept getById(String id);

}
